function get_info() {
    return new Promise((resolve, reject) => {
        let success_rate = Math.random();
        let timer = Math.floor(Math.random() * 1000 + 500);
        if (success_rate > 0.5) {
            let tmp_id = Math.floor(Math.random() * 14000 + 6000);
            console.log(tmp_id);
            setTimeout(() => {
                resolve(tmp_id);
            }, timer);
        } else {
            setTimeout(() => {
                reject('get_info Failed');
            }, timer);
        }
    });
}

function get_firstname() {
    first_name_list = ['Adam', 'Eric', 'Peter'];
    // TODO : generate a success rate

    // TODO : generate a timer

    // TODO : random select a item from list
    return new Promise((resolve, reject) => {
        let success_rate = Math.random();
        let timer = Math.floor(Math.random() * 1000 + 500);
        if (success_rate > 0.1) {
            first_name = first_name_list[timer%3];
            console.log(first_name);
            setTimeout(() => {
                resolve(first_name);
            }, timer);
        } else {
            setTimeout(() => {
                reject('get_info Failed');
            }, timer);
        }
    });
}

function get_lastname() {
    last_name_list = ['Jones', 'Smith', 'Johnson'];
    // TODO : generate a success rate

    // TODO : generate a timer

    // TODO : random select a item from list
    return new Promise((resolve, reject) => {
        let success_rate = Math.random();
        let timer = Math.floor(Math.random() * 1000 + 500);
        if (success_rate > 0.1) {
            last_name = last_name_list[timer%3];
            console.log(last_name);
            setTimeout(() => {
                resolve(last_name);
            }, timer);
        } else {
            setTimeout(() => {
                reject('get_info Failed');
            }, timer);
        }
    });
}

function get_username() {
    username_list = ['Toyz', 'Faker', 'Dinter'];
    // TODO : generate a success rate

    // TODO : generate a timer

    // TODO : random select a item from list
    return new Promise((resolve, reject) => {
        let success_rate = Math.random();
        let timer = Math.floor(Math.random() * 1000 + 500);
        if (success_rate > 0.1) {
            let username = username_list[timer%3];
            console.log(username);
            setTimeout(() => {
                resolve(username);
            }, timer);
        } else {
            setTimeout(() => {
                reject('get_info Failed');
            }, timer);
        }
    });
}

function get_email() {
    email_list = ['asdf@google.com', 'qwer@microsoft.com', 'zxcv@cs.nthu.edu.tw'];
    // TODO : generate a success rate

    // TODO : generate a timer

    // TODO : random select a item from list
    return new Promise((resolve, reject) => {
        let success_rate = Math.random();
        let timer = Math.floor(Math.random() * 1000 + 500);
        if (success_rate > 0.1) {
            let email = email_list[timer%3];
            console.log(email);
            setTimeout(() => {
                resolve(email);
            }, timer);
        } else {
            setTimeout(() => {
                reject('get_info Failed');
            }, timer);
        }
    });
}

function get_address() {
    address_list = ['1027 Alpha Avenue', '3132 Kidd Avenue', '876 Jefferson Street'];

    // TODO : generate a success rate

    // TODO : generate a timer

    // TODO : random select a item from list
    return new Promise((resolve, reject) => {
        let success_rate = Math.random();
        let timer = Math.floor(Math.random() * 1000 + 500);
        if (success_rate > 0.1) {
            let address = address_list[timer%3];
            console.log(address);
            setTimeout(() => {
                resolve(address);
            }, timer);
        } else {
            setTimeout(() => {
                reject('get_info Failed');
            }, timer);
        }
    });
}

function initApp() {
    var reSamplebtn = document.getElementById('resamplebtn');
    reSamplebtn.addEventListener('click', retrieve_data);
}

async function retrieve_data() {
    var txtInfoName = document.getElementById('user-info-name');
    var txtFirstName = document.getElementById('firstName');
    var txtLastName = document.getElementById('lastName');
    var txtUserName = document.getElementById('username');
    var txtEmail = document.getElementById('email');
    var txtAddress = document.getElementById('address');
    var boxReSample = document.getElementById('re-sample');
    txtInfoName.innerText = '-';
    txtFirstName.value = '-';
    txtLastName.value = '-';
    txtUserName.value = '-';
    txtEmail.value = '-';
    txtAddress.value = '-';
    try {
        // TODO : get_info first
        // TODO : call other function to get other data
        var timer = await get_info();
        document.getElementById("user-info-name").innerText = timer+"'s information";
        var lastname = get_lastname().then();
        var firstname = get_firstname().then();
        var username = get_username().then();
        var email = get_email().then();
        var address = get_address().then();
        Promise.all([lastname, firstname, username, email, address]).then(values =>{
            txtLastName.value = values[0];
            txtFirstName.value = values[1];
            txtUserName.value = values[2];
            txtEmail.value = values[3];
            txtAddress.value = values[4];
        }).catch(reason => { 
            document.getElementById("user-info-name").innerText = "Failed";
            if(boxReSample.checked){
                console.log("checked1");
                retrieve_data();
            }
        });

    } catch (e) {
        console.log(e);
        document.getElementById("user-info-name").innerText = "Failed";
        if(boxReSample.checked){
            console.log("checked2");
            retrieve_data();
        }
    }
}

window.onload = function() {
    initApp();
}